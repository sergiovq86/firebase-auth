package com.qastusoft.firebaseauth;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private TextView textView;
    private Button button;
    private FirebaseUser currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = findViewById(R.id.textView);
        button = findViewById(R.id.button);

        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();
        if (currentUser.isEmailVerified()) {
            userVerified();
        } else {
            changeUI();
        }

        Button logout = findViewById(R.id.button2);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAuth.signOut();
                reiniciarApp();
            }
        });
    }

    private void userVerified() {
        textView.setText("Bienvenido " + currentUser.getEmail());
        button.setText("Cambiar contraseña");
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nuevoMail = "";
                currentUser.updatePassword(nuevoMail).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(MainActivity.this, "Debes hacer login de nuevo", Toast.LENGTH_SHORT).show();
                            reiniciarApp();
                        } else {
                            Toast.makeText(MainActivity.this, "Error al cambiar la contraseña", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
    }

    private void changeUI() {
        textView.setText("Debes verificar tu dirección de correo: " + currentUser.getEmail());
        button.setText("Reenviar correo");
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentUser.sendEmailVerification();
                reiniciarApp();
            }
        });
    }

    private void reiniciarApp() {
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}
